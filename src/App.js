import React, { Component } from 'react';
import { Route, Switch, withRouter, Redirect } from 'react-router-dom';
import {connect} from 'react-redux';
import Header from './components/layout/header'
import Auth from './containers/auth';
import Orders from './containers/orders';
import './styles/App.css';

class App extends Component {
  render() {
    let routes = (
      <Switch>
        <Route path="/" exact component= {Auth}/>
        <Redirect to="/"/>
      </Switch>
    );

    if (this.props.isAuthenticated){
      routes = (
        <Switch>
          <Route path="/orders" component= {Orders}/>
          <Redirect to="/orders"/>
        </Switch>
      )
    }
    return (
      <div className='app'>
        <Header />
        {routes}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    isAuthenticated: state.auth.token !== null
  }
}

export default withRouter(connect(mapStateToProps)(App));
