import firebase from 'firebase';

var config = {
  apiKey: "AIzaSyD7zastXnQdK4QEMLf20kmKhkNahXuLX2o",
  authDomain: "liftit-app.firebaseapp.com",
  databaseURL: "https://liftit-app.firebaseio.com",
  projectId: "liftit-app",
  storageBucket: "liftit-app.appspot.com",
  messagingSenderId: "675327452045"
};

const fire = firebase.initializeApp(config)
export { fire }
