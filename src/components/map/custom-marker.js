import React from 'react'

import { CustomMarkerStyle } from './custom-marker-style.js';

const CustomMarker = props => (
  <div style={CustomMarkerStyle}>
    {props.text}
  </div>
)


export default CustomMarker
