const M_WIDTH = 20;
const M_HEIGHT = 20;

const CustomMarkerStyle = {
  position: 'absolute',
  width: M_WIDTH,
  height: M_HEIGHT,
  left: -M_WIDTH / 2,
  top: -M_HEIGHT / 2,

  border: '5px solid #f44336',
  borderRadius: M_HEIGHT,
  backgroundColor: 'white',
  textAlign: 'center',
  color: '#3f51b5',
  fontSize: 16,
  fontWeight: 'bold',
  padding: 4
};

export { CustomMarkerStyle };
