import React from 'react';
import PropTypes from 'prop-types';
import '../../styles/components/ui/input-form.css';

const InputForm = props => (
  <div className='input-form'>
    <label htmlFor={props.name}>{props.label}</label> 
    <input
      type={props.type}
      name={props.name}
      value={props.value}
      required
      placeholder={props.placeholder}
      onChange={(event) => props.handleInputValue(event)}
    />
  </div>
)

InputForm.propTypes = {
  name: PropTypes.string,
  label: PropTypes.string,
  type: PropTypes.string,
  value: PropTypes.string,
  placeholder:PropTypes.string,
  handleInputValue: PropTypes.func
}

InputForm.defaultProps = {
  placeholder: ''
}

export default InputForm;
