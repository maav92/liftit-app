import React from 'react';
import PropTypes from 'prop-types';
import '../../styles/components/ui/textarea-form.css';

const TextAreaForm = props => (
  <div className='textarea-form'>
    <label htmlFor={props.name}>{props.label}</label> 
    <textarea
      type={props.type}
      name={props.name}
      value={props.value}
      required
      placeholder={props.placeholder}
      onChange={(event) => props.setDescription(event)}
    />
  </div>
)

TextAreaForm.propTypes = {
  name: PropTypes.string,
  label: PropTypes.string,
  type: PropTypes.string,
  value: PropTypes.string,
  placeholder:PropTypes.string,
  setDescription: PropTypes.func
}

TextAreaForm.defaultProps = {
  placeholder: ''
}

export default TextAreaForm;
