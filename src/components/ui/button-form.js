import React from 'react'
import PropTypes from 'prop-types'
import '../../styles/components/ui/button-form.css';

const Button = props => (
  <button
    type='submit'
    className={`button-form ${props.action}`}>
    { props.label }
  </button>
)

Button.propTypes = {
  action: PropTypes.string,
  label: PropTypes.string
}

export default Button
