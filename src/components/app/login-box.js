import React from 'react';
import PropTypes from 'prop-types';
import InputForm from '../ui/input-form';
import ButtonForm from '../ui/button-form';
import '../../styles/components/app/login-box.css';

const LoginBox = props => (
  <div className='login-box-container'>
    <h1>Iniciar Sesion</h1>
    <form onSubmit={props.handleSubmit}>
      <InputForm
        placeholder='Username'
        handleInputValue={props.handleInputValue}
        name='username'
        value={props.inputs.username}
      />
      <InputForm
        placeholder='Password'
        type='password'
        handleInputValue={props.handleInputValue}
        name='password'
        value={props.inputs.password}
      />
      <span className='login-error-msg'>
        {props.errorMsg}
      </span>
      <ButtonForm
        label='Enter'
        action='submit'
      />
    </form>
  </div>
)

LoginBox.propTypes = {
  inputs: PropTypes.object,
  handleSubmit: PropTypes.func,
  handleInputValue: PropTypes.func
}

export default LoginBox;
