import React from 'react';
import '../../styles/components/layout/header.css';

const Header = props => (
  <header className="App-header">
    <h1 className="App-title">Liftit App</h1>
  </header>
)

export default Header;
