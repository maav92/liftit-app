import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import TextAreaForm from '../../components/ui/textarea-form';
import LocationSearchInput from '../../components/ui/location-search-input';
import * as actionCreator from '../../store/actions/index';
import GoogleMap from 'google-map-react';
import CustomMarker from '../../components/map/custom-marker';
import '../../styles/containers/orders/index.css';

class Orders extends Component {
  constructor (props) {
    super(props)
    this.state = {
      value: ''
    }
    this.handleSearch = this.handleSearch.bind(this)
    this.handleCreate = this.handleCreate.bind(this)
  }

  handleSearch () {
    const { fetchInformation, originLat, originLng, destinationLat, destinationLng } = this.props
    fetchInformation(originLat, originLng, destinationLat, destinationLng)
  }

  handleCreate () {
    const { distance, time, originAddress, destinationAddress, description, addOrder, cleanForm } = this.props
    const data = {
      originAddress,
      destinationAddress,
      distance,
      time,
      description
    }
    addOrder(data)
    alert('The order was saved successfully');
    cleanForm()

  }

  render() {
    const { distance, time, status, setAddressInfo, setDescription, description } = this.props
    const { originLat, originLng, destinationLat, destinationLng } = this.props
    const { originAddress, destinationAddress } = this.props    
    const center = [
      originLat, originLng
    ]
    return (
      <div className="create-order-container">
        <div className='order-form'>
          <h1>Create Order</h1>
          <LocationSearchInput
            setAddressInfo={setAddressInfo}
            currentAddress={originAddress}
            type='origin'
          />
          <LocationSearchInput
            setAddressInfo={setAddressInfo}
            currentAddress={destinationAddress}
            type='destination'
          />
          <TextAreaForm
            placeholder='Description'
            setDescription={setDescription}
            name='description'
            value={description}
          />
          <button className='button-form' onClick={this.handleSearch}>Consult</button>
        </div>
        <div className='results-container'>
          {
            status === 'OK'
             ? <Fragment> 
                <h2>Results:</h2>
                <span><strong>From (A): </strong>{originAddress}</span><br />
                <span><strong>To (B): </strong>{destinationAddress}</span><br />
                <span><strong>Distance: </strong>{distance}</span><br />
                <span><strong>Time: </strong>{time}</span>
                <div className='map-container'>
                  <GoogleMap
                    key='AIzaSyC6EJEMa2M_Hck4jGZjVFLRJKExfr_pcU8'
                    center={center}
                    zoom={5}>
                    <CustomMarker lat={originLat} lng={originLng} text={'A'} />
                    <CustomMarker lat={destinationLat} lng={destinationLng} text={'B'} />
                  </GoogleMap>
                </div>
                <button className='button-form' onClick={this.handleCreate}>Confirm Order</button>
              </Fragment>
            : <span>{status}</span>
          }
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    originAddress: state.order.originAddress,
    originLat: state.order.originLat,
    originLng: state.order.originLng,
    destinationAddress: state.order.destinationAddress,
    destinationLat: state.order.destinationLat,
    destinationLng: state.order.destinationLng,
    distance: state.order.distance,
    time: state.order.time,
    status: state.order.status,
    description: state.order.description
  }
}

const mapDispatchToProps=(dispatch) => {
  return {
    setDescription: (event) => {
      event.persist()
      const { target } = event
      dispatch(actionCreator.setDescription(target.value))
    },
    addOrder: (data) => dispatch(actionCreator.addOrder(data)),
    cleanForm: () =>  dispatch(actionCreator.cleanForm()),
    setAddressInfo: (type, address, lat, lng) => dispatch(actionCreator.setAddressInfo(type, address, lat, lng)),
    fetchInformation: (originLat, originLng, destinationLat, destinationLng) => dispatch(actionCreator.fetchInformation(originLat, originLng, destinationLat, destinationLng))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Orders);
