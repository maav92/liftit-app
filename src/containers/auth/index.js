import React, { Component } from 'react';
import { connect } from 'react-redux';
import LoginBox from '../../components/app/login-box';
import * as actionCreator from '../../store/actions/index';
import '../../styles/containers/auth/index.css';

class Auth extends Component {
  constructor (props) {
    super(props)
    this.state = {
      inputs: {
        username: '',
        password: ''
      }
    }
    this.handleSubmit = this.handleSubmit.bind(this)
    this.handleInputValue = this.handleInputValue.bind(this)
  }

  handleSubmit (event) {
    event.preventDefault();
    const { inputs } = this.state
    this.props.login(inputs.username, inputs.password)
  }

  handleInputValue ({target: { name, value }}) {
    const { inputs } = this.state
    inputs[name] = value
    this.setState({ inputs })
  }

  render() {
    return (
      <div className="login-box">
        <LoginBox
          handleSubmit={this.handleSubmit}
          handleInputValue={this.handleInputValue}
          inputs={this.state.inputs}
          errorMsg={this.props.authError}
        />
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    authError: state.auth.error
  }
}

const mapDispatchToProps=(dispatch) => {
  return {
    login: (email, password) => dispatch(actionCreator.login(email, password))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Auth);

