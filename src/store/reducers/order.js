import _ from 'lodash';
import * as actionTypes from '../actions/actionTypes';
import {updateObject} from '../utils';

const initialState = {
    originAddress: null,
    originLat: null,
    originLng: null,    
    destinationAddress: null,    
    destinationLat: null,
    destinationLng: null, 
    description: '',   
    distance: null,
    time: null,
    status: null,
    error: null,
    loading: false,
    orders: []
}

const reducer = (state=initialState, action) => {
  switch (action.type) {
    case actionTypes.SET_DESCRIPTION:
      return updateObject({
        ...state,
        description: action.description
      });
    case actionTypes.SET_ADDRESS_INFO:
      var info = {}
      if (action.typeAddress === 'origin') {
        info = {
          ...state,
          originAddress: action.address,
          originLat: action.lat,
          originLng: action.lng
        }
      } else if (action.typeAddress === 'destination'){
        info = {
          ...state,
          destinationAddress: action.address,
          destinationLat: action.lat,
          destinationLng: action.lng
        }
      }

      return updateObject(info);
    case actionTypes.FETCH_INFO_START:
      return updateObject(state, {error: null, loading: true});
    case actionTypes.FETCH_INFO_SUCCESS:
      return updateObject({
        ...state,
        distance: action.distance,
        time: action.time,
        status: action.status
      });
    case actionTypes.FETCH_INFO_FAIL:
      return updateObject(state, {error: action.error, loading: false});
    case actionTypes.ADD_ORDER:
      const newItems = _.concat(state.orders, action.data)
      return {
        ...state,
        orders: newItems
      }
    case actionTypes.CLEAN_FORM:
      return {
        ...initialState,
        orders: state.orders
      }
    default:
        return state 
  }
};

export default reducer;