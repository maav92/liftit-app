import * as actionTypes from './actionTypes';

export const setAddressInfo = (typeAddress, address, lat, lng) => {
  return {
      type: actionTypes.SET_ADDRESS_INFO,
      typeAddress: typeAddress,
      address,    
      lat,
      lng
  }
}

export const setDescription = (description) => {
  return {
      type: actionTypes.SET_DESCRIPTION,
      description
  }
}

export const addOrder = (data) => {
  return {
      type: actionTypes.ADD_ORDER,
      data
  }
}

export const cleanForm = () => {
  return {
      type: actionTypes.CLEAN_FORM
  }
}

export const fetchInfoSuccess = (distance, time, status) => {
  return {
      type: actionTypes.FETCH_INFO_SUCCESS,
      distance: distance,
      time: time,
      status: status
  }
}

export const fetchInfoStart = () => {
  return {
      type: actionTypes.FETCH_INFO_START
  }
}

export const fetchInfoFail = (error) => {
  return {
      type: actionTypes.FETCH_INFO_FAIL,
      error: error
  }
}

export const fetchInformation = (originLat, originLng, destinationLat, destinationLng) => {
  return dispatch => {
    dispatch(fetchInfoStart());
    const google = window.google;
    var origin1 = new google.maps.LatLng(originLat, originLng);
    var destinationB = new google.maps.LatLng(destinationLat, destinationLng);

    var service = new google.maps.DistanceMatrixService();
    service.getDistanceMatrix(
      {
        origins: [origin1],
        destinations: [destinationB],
        travelMode: 'DRIVING',
      }, callback);

    function callback(response, status) {
      const info = response.rows[0].elements[0]
      dispatch(
        fetchInfoSuccess (
        info.distance ? info.distance.text : null,
        info.duration ? info.duration.text : null,
        info.status || null
      ))
    }
  }
}
