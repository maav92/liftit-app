
import { fire } from '../../firebase-instance'
import * as actionTypes from './actionTypes';

export const authStart = () => {
  return {
      type: actionTypes.AUTH_START
  };
};

export const authSuccess = (token, userId) => {
  return {
      type: actionTypes.AUTH_SUCCESS,
      idToken: token,
      userId: userId
  }
}

export const authFail = (error) => {
  return {
      type: actionTypes.AUTH_FAIL,
      error: error
  }
}

export const logout = () => {
  return {
      type: actionTypes.AUTH_LOGOUT
  }
}

export const login = (email, password) => {
  return dispatch => {
    dispatch(authStart());
    fire.auth().signInWithEmailAndPassword(email, password).then(function(response) {
      var sessionToken = response.user.refreshToken
      var userId = response.user.uid
      dispatch(authSuccess(sessionToken, userId));
    }).catch(function(error) {
      var errorMessage = error.message;
      dispatch(authFail(errorMessage));
    });
  }
}
